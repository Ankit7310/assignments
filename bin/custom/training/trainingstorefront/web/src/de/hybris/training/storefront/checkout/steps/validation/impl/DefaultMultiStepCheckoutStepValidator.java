package de.hybris.training.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import java.util.List;

public class DefaultMultiStepCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOGGER = Logger.getLogger(DefaultMultiStepCheckoutStepValidator.class);
// private BaseStoreService baseStoreService;
//
// public BaseStoreService getBaseStoreService() {
//    return baseStoreService;
// }
//
// public void setBaseStoreService(BaseStoreService baseStoreService) {
//    this.baseStoreService = baseStoreService;
// }

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
		{
			if(validationOnWeight(cartData))
				return getCheckoutFacade().hasShippingItems() ? ValidationResults.SUCCESS
						: ValidationResults.REDIRECT_TO_PICKUP_LOCATION;
		}
		LOGGER.info("Missing, empty or unsupported cart");
		return ValidationResults.FAILED;
	}
	private boolean validationOnWeight(CartData cartData) {
		Double sum=0.0;
		Double threshold=5000.0;
		//BaseStoreModel store = order.getStore();
		for (OrderEntryData data: cartData.getEntries()) {
			//sum += data.getTotalWeight();
		}
		if(sum>threshold){
			LOGGER.info("Cart Weight is greater than Threshold");
			return false;
		}
		return true;
	}
}