package de.hybris.training.core.dao.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.impl.DefaultProductDao;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.training.core.dao.CustomProductDAO;
import de.hybris.training.core.model.BrandCarouselComponentModel;

import java.util.List;

public class CustomProductDaoImpl extends DefaultProductDao implements CustomProductDAO {
    public CustomProductDaoImpl(String typecode) {
        super(typecode);
    }

    @Override
    public List<ProductModel> findAllProductsWhichAreNew() {
            final String queryString = String.format("SELECT {" + ProductModel.PK + "} FROM {" + ProductModel._TYPECODE + "} WHERE {" + ProductModel.NEWPRODUCT + "} = "+ Boolean.TRUE);
            final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
            final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);

            return result.getResult();

    }

    @Override
    public CategoryModel findCategoryByCategoryCode(String code) {
        final String queryString = String.format("SELECT {" + CategoryModel.PK + "} FROM {" + CategoryModel._TYPECODE + "} WHERE  {"+ CategoryModel.CODE +"}=?code");
        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
        query.addQueryParameter("code",code);
        final SearchResult<CategoryModel> result = getFlexibleSearchService().search(query);

        return result.getResult().get(0);

    }

    @Override
    public List<ProductModel> getProductsByBrand(String brand) {

        BrandCarouselComponentModel brandCarouselComponentModel = getMaxNumberByBrand(brand);
        final String queryString = String.format("SELECT {" + ProductModel.PK + "} FROM {" + ProductModel._TYPECODE + "} WHERE  {"+ ProductModel.BRAND +"}=?brand");
        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
        query.addQueryParameter("brand",brand);
        query.setCount(brandCarouselComponentModel.getMaxNumber());
        final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);

        return result.getResult();
        }
    protected BrandCarouselComponentModel getMaxNumberByBrand(String brand){

        final String queryString = String.format("SELECT {" + BrandCarouselComponentModel.PK + "} FROM {" + BrandCarouselComponentModel._TYPECODE + "} WHERE  {"+ BrandCarouselComponentModel.BRAND +"}=?brand");
        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
        query.addQueryParameter("brand",brand);
        final SearchResult<BrandCarouselComponentModel> result = getFlexibleSearchService().search(query);
        if(result != null){
            return result.getResult().get(0);
        }
        else{
            throw new UnknownIdentifierException("No Brand is associated with this " + brand);
        }

    }
}
