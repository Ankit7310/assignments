package de.hybris.training.core.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.ProductDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

public interface CustomProductDAO extends ProductDao {
    List<ProductModel> findAllProductsWhichAreNew();
    CategoryModel findCategoryByCategoryCode(String code);
    List<ProductModel> getProductsByBrand(String brand);
}

