package de.hybris.training.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;

public class WeightValueResolver extends AbstractValueResolver<ProductModel, Object, Object> {

    @Override
    protected void addFieldValues(InputDocument inputDocument, IndexerBatchContext indexerBatchContext, IndexedProperty indexedProperty, ProductModel productModel, ValueResolverContext<Object, Object> valueResolverContext) throws FieldValueProviderException {
        if(productModel.getWeight() != null){
            inputDocument.addField(indexedProperty, productModel.getWeight()*1000, valueResolverContext.getFieldQualifier());
        }
        else{
            inputDocument.addField(indexedProperty, 0, valueResolverContext.getFieldQualifier());

        }
    }
}