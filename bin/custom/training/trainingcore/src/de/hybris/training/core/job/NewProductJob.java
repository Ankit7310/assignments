package de.hybris.training.core.job;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.jalo.NewProductCronJob;
import de.hybris.platform.cronjob.model.NewProductCronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.training.core.dao.CustomProductDAO;
import de.hybris.training.core.service.CustomProductService;
import org.springframework.util.CollectionUtils;
import java.util.List;

public class NewProductJob extends AbstractJobPerformable<NewProductCronJobModel> {
    public static final String SITE_UID = "electronics";
    private CustomProductService customProductService;
    private ModelService modelService;
    @Override
    public PerformResult perform(NewProductCronJobModel newProductCronJobModel) {
        final List<ProductModel> productModelList = customProductService.findAllProductsWhichAreNew();
        if (!CollectionUtils.isEmpty(productModelList)){
            final CategoryModel categoryModelList = customProductService.findCategoryByCategoryCode("NewArrivals");
            categoryModelList.setProducts(productModelList);
        }
        return null;
    }

    public CustomProductService getCustomProductService() {

        return customProductService;

    }

    public void setCustomProductService(CustomProductService customProductService) {

        this.customProductService = customProductService;

    }

    public ModelService getModelService() {

        return modelService;

    }

    @Override

    public void setModelService(ModelService modelService) {

        this.modelService = modelService;

    }

}

