/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 13-Jan-2021, 2:23:56 PM                     ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.training.core.jalo;

import de.hybris.platform.cms2.jalo.contents.components.CMSLinkComponent;
import de.hybris.platform.cms2lib.components.ProductCarouselComponent;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.training.core.constants.TrainingCoreConstants;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.training.core.jalo.BrandCarouselComponent BrandCarouselComponent}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedBrandCarouselComponent extends ProductCarouselComponent
{
	/** Qualifier of the <code>BrandCarouselComponent.description</code> attribute **/
	public static final String DESCRIPTION = "description";
	/** Qualifier of the <code>BrandCarouselComponent.link</code> attribute **/
	public static final String LINK = "link";
	/** Qualifier of the <code>BrandCarouselComponent.brand</code> attribute **/
	public static final String BRAND = "brand";
	/** Qualifier of the <code>BrandCarouselComponent.maxNumber</code> attribute **/
	public static final String MAXNUMBER = "maxNumber";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(ProductCarouselComponent.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(DESCRIPTION, AttributeMode.INITIAL);
		tmp.put(LINK, AttributeMode.INITIAL);
		tmp.put(BRAND, AttributeMode.INITIAL);
		tmp.put(MAXNUMBER, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.brand</code> attribute.
	 * @return the brand - Brand of BrandCarouselComponent
	 */
	public String getBrand(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedBrandCarouselComponent.getBrand requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, BRAND);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.brand</code> attribute.
	 * @return the brand - Brand of BrandCarouselComponent
	 */
	public String getBrand()
	{
		return getBrand( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.brand</code> attribute. 
	 * @return the localized brand - Brand of BrandCarouselComponent
	 */
	public Map<Language,String> getAllBrand(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,BRAND,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.brand</code> attribute. 
	 * @return the localized brand - Brand of BrandCarouselComponent
	 */
	public Map<Language,String> getAllBrand()
	{
		return getAllBrand( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.brand</code> attribute. 
	 * @param value the brand - Brand of BrandCarouselComponent
	 */
	public void setBrand(final SessionContext ctx, final String value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedBrandCarouselComponent.setBrand requires a session language", 0 );
		}
		setLocalizedProperty(ctx, BRAND,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.brand</code> attribute. 
	 * @param value the brand - Brand of BrandCarouselComponent
	 */
	public void setBrand(final String value)
	{
		setBrand( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.brand</code> attribute. 
	 * @param value the brand - Brand of BrandCarouselComponent
	 */
	public void setAllBrand(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,BRAND,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.brand</code> attribute. 
	 * @param value the brand - Brand of BrandCarouselComponent
	 */
	public void setAllBrand(final Map<Language,String> value)
	{
		setAllBrand( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.description</code> attribute.
	 * @return the description - Description of BrandCarouselComponent
	 */
	public String getDescription(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedBrandCarouselComponent.getDescription requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, DESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.description</code> attribute.
	 * @return the description - Description of BrandCarouselComponent
	 */
	public String getDescription()
	{
		return getDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.description</code> attribute. 
	 * @return the localized description - Description of BrandCarouselComponent
	 */
	public Map<Language,String> getAllDescription(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,DESCRIPTION,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.description</code> attribute. 
	 * @return the localized description - Description of BrandCarouselComponent
	 */
	public Map<Language,String> getAllDescription()
	{
		return getAllDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.description</code> attribute. 
	 * @param value the description - Description of BrandCarouselComponent
	 */
	public void setDescription(final SessionContext ctx, final String value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedBrandCarouselComponent.setDescription requires a session language", 0 );
		}
		setLocalizedProperty(ctx, DESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.description</code> attribute. 
	 * @param value the description - Description of BrandCarouselComponent
	 */
	public void setDescription(final String value)
	{
		setDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.description</code> attribute. 
	 * @param value the description - Description of BrandCarouselComponent
	 */
	public void setAllDescription(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,DESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.description</code> attribute. 
	 * @param value the description - Description of BrandCarouselComponent
	 */
	public void setAllDescription(final Map<Language,String> value)
	{
		setAllDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.link</code> attribute.
	 * @return the link - Link of BrandCarouselComponent
	 */
	public CMSLinkComponent getLink(final SessionContext ctx)
	{
		return (CMSLinkComponent)getProperty( ctx, LINK);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.link</code> attribute.
	 * @return the link - Link of BrandCarouselComponent
	 */
	public CMSLinkComponent getLink()
	{
		return getLink( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.link</code> attribute. 
	 * @param value the link - Link of BrandCarouselComponent
	 */
	public void setLink(final SessionContext ctx, final CMSLinkComponent value)
	{
		setProperty(ctx, LINK,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.link</code> attribute. 
	 * @param value the link - Link of BrandCarouselComponent
	 */
	public void setLink(final CMSLinkComponent value)
	{
		setLink( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.maxNumber</code> attribute.
	 * @return the maxNumber - MaxNumber of BrandCarouselComponent
	 */
	public Integer getMaxNumber(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedBrandCarouselComponent.getMaxNumber requires a session language", 0 );
		}
		return (Integer)getLocalizedProperty( ctx, MAXNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.maxNumber</code> attribute.
	 * @return the maxNumber - MaxNumber of BrandCarouselComponent
	 */
	public Integer getMaxNumber()
	{
		return getMaxNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.maxNumber</code> attribute. 
	 * @return the maxNumber - MaxNumber of BrandCarouselComponent
	 */
	public int getMaxNumberAsPrimitive(final SessionContext ctx)
	{
		Integer value = getMaxNumber( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.maxNumber</code> attribute. 
	 * @return the maxNumber - MaxNumber of BrandCarouselComponent
	 */
	public int getMaxNumberAsPrimitive()
	{
		return getMaxNumberAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.maxNumber</code> attribute. 
	 * @return the localized maxNumber - MaxNumber of BrandCarouselComponent
	 */
	public Map<Language,Integer> getAllMaxNumber(final SessionContext ctx)
	{
		return (Map<Language,Integer>)getAllLocalizedProperties(ctx,MAXNUMBER,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BrandCarouselComponent.maxNumber</code> attribute. 
	 * @return the localized maxNumber - MaxNumber of BrandCarouselComponent
	 */
	public Map<Language,Integer> getAllMaxNumber()
	{
		return getAllMaxNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.maxNumber</code> attribute. 
	 * @param value the maxNumber - MaxNumber of BrandCarouselComponent
	 */
	public void setMaxNumber(final SessionContext ctx, final Integer value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedBrandCarouselComponent.setMaxNumber requires a session language", 0 );
		}
		setLocalizedProperty(ctx, MAXNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.maxNumber</code> attribute. 
	 * @param value the maxNumber - MaxNumber of BrandCarouselComponent
	 */
	public void setMaxNumber(final Integer value)
	{
		setMaxNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.maxNumber</code> attribute. 
	 * @param value the maxNumber - MaxNumber of BrandCarouselComponent
	 */
	public void setMaxNumber(final SessionContext ctx, final int value)
	{
		setMaxNumber( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.maxNumber</code> attribute. 
	 * @param value the maxNumber - MaxNumber of BrandCarouselComponent
	 */
	public void setMaxNumber(final int value)
	{
		setMaxNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.maxNumber</code> attribute. 
	 * @param value the maxNumber - MaxNumber of BrandCarouselComponent
	 */
	public void setAllMaxNumber(final SessionContext ctx, final Map<Language,Integer> value)
	{
		setAllLocalizedProperties(ctx,MAXNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BrandCarouselComponent.maxNumber</code> attribute. 
	 * @param value the maxNumber - MaxNumber of BrandCarouselComponent
	 */
	public void setAllMaxNumber(final Map<Language,Integer> value)
	{
		setAllMaxNumber( getSession().getSessionContext(), value );
	}
	
}
