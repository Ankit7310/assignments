/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 13-Jan-2021, 2:23:56 PM                     ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.training.core.jalo;

import de.hybris.platform.cronjob.jalo.NewProductCronJob;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.link.Link;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.jalo.user.Customer;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.util.Utilities;
import de.hybris.training.core.constants.TrainingCoreConstants;
import de.hybris.training.core.jalo.ApparelProduct;
import de.hybris.training.core.jalo.ApparelSizeVariantProduct;
import de.hybris.training.core.jalo.ApparelStyleVariantProduct;
import de.hybris.training.core.jalo.BrandCarouselComponent;
import de.hybris.training.core.jalo.ElectronicsColorVariantProduct;
import de.hybris.training.core.jalo.SalesRepresentative;
import de.hybris.training.core.jalo.Service;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Generated class for type <code>TrainingCoreManager</code>.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedTrainingCoreManager extends Extension
{
	/** Relation ordering override parameter constants for Product2Service from ((trainingcore))*/
	protected static String PRODUCT2SERVICE_SRC_ORDERED = "relation.Product2Service.source.ordered";
	protected static String PRODUCT2SERVICE_TGT_ORDERED = "relation.Product2Service.target.ordered";
	/** Relation disable markmodifed parameter constants for Product2Service from ((trainingcore))*/
	protected static String PRODUCT2SERVICE_MARKMODIFIED = "relation.Product2Service.markmodified";
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put("brand", AttributeMode.INITIAL);
		tmp.put("weight", AttributeMode.INITIAL);
		tmp.put("newProduct", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.product.Product", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("totalWeight", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.order.AbstractOrderEntry", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("salesRepresentatives", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.user.Customer", Collections.unmodifiableMap(tmp));
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.brand</code> attribute.
	 * @return the brand - Brand of the product
	 */
	public String getBrand(final SessionContext ctx, final Product item)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedProduct.getBrand requires a session language", 0 );
		}
		return (String)item.getLocalizedProperty( ctx, TrainingCoreConstants.Attributes.Product.BRAND);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.brand</code> attribute.
	 * @return the brand - Brand of the product
	 */
	public String getBrand(final Product item)
	{
		return getBrand( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.brand</code> attribute. 
	 * @return the localized brand - Brand of the product
	 */
	public Map<Language,String> getAllBrand(final SessionContext ctx, final Product item)
	{
		return (Map<Language,String>)item.getAllLocalizedProperties(ctx,TrainingCoreConstants.Attributes.Product.BRAND,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.brand</code> attribute. 
	 * @return the localized brand - Brand of the product
	 */
	public Map<Language,String> getAllBrand(final Product item)
	{
		return getAllBrand( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.brand</code> attribute. 
	 * @param value the brand - Brand of the product
	 */
	public void setBrand(final SessionContext ctx, final Product item, final String value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedProduct.setBrand requires a session language", 0 );
		}
		item.setLocalizedProperty(ctx, TrainingCoreConstants.Attributes.Product.BRAND,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.brand</code> attribute. 
	 * @param value the brand - Brand of the product
	 */
	public void setBrand(final Product item, final String value)
	{
		setBrand( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.brand</code> attribute. 
	 * @param value the brand - Brand of the product
	 */
	public void setAllBrand(final SessionContext ctx, final Product item, final Map<Language,String> value)
	{
		item.setAllLocalizedProperties(ctx,TrainingCoreConstants.Attributes.Product.BRAND,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.brand</code> attribute. 
	 * @param value the brand - Brand of the product
	 */
	public void setAllBrand(final Product item, final Map<Language,String> value)
	{
		setAllBrand( getSession().getSessionContext(), item, value );
	}
	
	public ApparelProduct createApparelProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TrainingCoreConstants.TC.APPARELPRODUCT );
			return (ApparelProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelProduct createApparelProduct(final Map attributeValues)
	{
		return createApparelProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public ApparelSizeVariantProduct createApparelSizeVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TrainingCoreConstants.TC.APPARELSIZEVARIANTPRODUCT );
			return (ApparelSizeVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelSizeVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelSizeVariantProduct createApparelSizeVariantProduct(final Map attributeValues)
	{
		return createApparelSizeVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public ApparelStyleVariantProduct createApparelStyleVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TrainingCoreConstants.TC.APPARELSTYLEVARIANTPRODUCT );
			return (ApparelStyleVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelStyleVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelStyleVariantProduct createApparelStyleVariantProduct(final Map attributeValues)
	{
		return createApparelStyleVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public BrandCarouselComponent createBrandCarouselComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TrainingCoreConstants.TC.BRANDCAROUSELCOMPONENT );
			return (BrandCarouselComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating BrandCarouselComponent : "+e.getMessage(), 0 );
		}
	}
	
	public BrandCarouselComponent createBrandCarouselComponent(final Map attributeValues)
	{
		return createBrandCarouselComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public ElectronicsColorVariantProduct createElectronicsColorVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TrainingCoreConstants.TC.ELECTRONICSCOLORVARIANTPRODUCT );
			return (ElectronicsColorVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ElectronicsColorVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ElectronicsColorVariantProduct createElectronicsColorVariantProduct(final Map attributeValues)
	{
		return createElectronicsColorVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public NewProductCronJob createNewProductCronJob(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TrainingCoreConstants.TC.NEWPRODUCTCRONJOB );
			return (NewProductCronJob)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating NewProductCronJob : "+e.getMessage(), 0 );
		}
	}
	
	public NewProductCronJob createNewProductCronJob(final Map attributeValues)
	{
		return createNewProductCronJob( getSession().getSessionContext(), attributeValues );
	}
	
	public SalesRepresentative createSalesRepresentative(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TrainingCoreConstants.TC.SALESREPRESENTATIVE );
			return (SalesRepresentative)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SalesRepresentative : "+e.getMessage(), 0 );
		}
	}
	
	public SalesRepresentative createSalesRepresentative(final Map attributeValues)
	{
		return createSalesRepresentative( getSession().getSessionContext(), attributeValues );
	}
	
	public Service createService(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TrainingCoreConstants.TC.SERVICE );
			return (Service)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating Service : "+e.getMessage(), 0 );
		}
	}
	
	public Service createService(final Map attributeValues)
	{
		return createService( getSession().getSessionContext(), attributeValues );
	}
	
	@Override
	public String getName()
	{
		return TrainingCoreConstants.EXTENSIONNAME;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.newProduct</code> attribute.
	 * @return the newProduct - New product
	 */
	public Boolean isNewProduct(final SessionContext ctx, final Product item)
	{
		return (Boolean)item.getProperty( ctx, TrainingCoreConstants.Attributes.Product.NEWPRODUCT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.newProduct</code> attribute.
	 * @return the newProduct - New product
	 */
	public Boolean isNewProduct(final Product item)
	{
		return isNewProduct( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.newProduct</code> attribute. 
	 * @return the newProduct - New product
	 */
	public boolean isNewProductAsPrimitive(final SessionContext ctx, final Product item)
	{
		Boolean value = isNewProduct( ctx,item );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.newProduct</code> attribute. 
	 * @return the newProduct - New product
	 */
	public boolean isNewProductAsPrimitive(final Product item)
	{
		return isNewProductAsPrimitive( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.newProduct</code> attribute. 
	 * @param value the newProduct - New product
	 */
	public void setNewProduct(final SessionContext ctx, final Product item, final Boolean value)
	{
		item.setProperty(ctx, TrainingCoreConstants.Attributes.Product.NEWPRODUCT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.newProduct</code> attribute. 
	 * @param value the newProduct - New product
	 */
	public void setNewProduct(final Product item, final Boolean value)
	{
		setNewProduct( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.newProduct</code> attribute. 
	 * @param value the newProduct - New product
	 */
	public void setNewProduct(final SessionContext ctx, final Product item, final boolean value)
	{
		setNewProduct( ctx, item, Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.newProduct</code> attribute. 
	 * @param value the newProduct - New product
	 */
	public void setNewProduct(final Product item, final boolean value)
	{
		setNewProduct( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.salesRepresentatives</code> attribute.
	 * @return the salesRepresentatives
	 */
	public SalesRepresentative getSalesRepresentatives(final SessionContext ctx, final Customer item)
	{
		return (SalesRepresentative)item.getProperty( ctx, TrainingCoreConstants.Attributes.Customer.SALESREPRESENTATIVES);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.salesRepresentatives</code> attribute.
	 * @return the salesRepresentatives
	 */
	public SalesRepresentative getSalesRepresentatives(final Customer item)
	{
		return getSalesRepresentatives( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.salesRepresentatives</code> attribute. 
	 * @param value the salesRepresentatives
	 */
	public void setSalesRepresentatives(final SessionContext ctx, final Customer item, final SalesRepresentative value)
	{
		item.setProperty(ctx, TrainingCoreConstants.Attributes.Customer.SALESREPRESENTATIVES,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.salesRepresentatives</code> attribute. 
	 * @param value the salesRepresentatives
	 */
	public void setSalesRepresentatives(final Customer item, final SalesRepresentative value)
	{
		setSalesRepresentatives( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.services</code> attribute.
	 * @return the services
	 */
	public Set<Service> getServices(final SessionContext ctx, final Product item)
	{
		final List<Service> items = item.getLinkedItems( 
			ctx,
			true,
			TrainingCoreConstants.Relations.PRODUCT2SERVICE,
			"Service",
			null,
			false,
			false
		);
		return new LinkedHashSet<Service>(items);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.services</code> attribute.
	 * @return the services
	 */
	public Set<Service> getServices(final Product item)
	{
		return getServices( getSession().getSessionContext(), item );
	}
	
	public long getServicesCount(final SessionContext ctx, final Product item)
	{
		return item.getLinkedItemsCount(
			ctx,
			true,
			TrainingCoreConstants.Relations.PRODUCT2SERVICE,
			"Service",
			null
		);
	}
	
	public long getServicesCount(final Product item)
	{
		return getServicesCount( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.services</code> attribute. 
	 * @param value the services
	 */
	public void setServices(final SessionContext ctx, final Product item, final Set<Service> value)
	{
		item.setLinkedItems( 
			ctx,
			true,
			TrainingCoreConstants.Relations.PRODUCT2SERVICE,
			null,
			value,
			false,
			false,
			Utilities.getMarkModifiedOverride(PRODUCT2SERVICE_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.services</code> attribute. 
	 * @param value the services
	 */
	public void setServices(final Product item, final Set<Service> value)
	{
		setServices( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to services. 
	 * @param value the item to add to services
	 */
	public void addToServices(final SessionContext ctx, final Product item, final Service value)
	{
		item.addLinkedItems( 
			ctx,
			true,
			TrainingCoreConstants.Relations.PRODUCT2SERVICE,
			null,
			Collections.singletonList(value),
			false,
			false,
			Utilities.getMarkModifiedOverride(PRODUCT2SERVICE_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to services. 
	 * @param value the item to add to services
	 */
	public void addToServices(final Product item, final Service value)
	{
		addToServices( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from services. 
	 * @param value the item to remove from services
	 */
	public void removeFromServices(final SessionContext ctx, final Product item, final Service value)
	{
		item.removeLinkedItems( 
			ctx,
			true,
			TrainingCoreConstants.Relations.PRODUCT2SERVICE,
			null,
			Collections.singletonList(value),
			false,
			false,
			Utilities.getMarkModifiedOverride(PRODUCT2SERVICE_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from services. 
	 * @param value the item to remove from services
	 */
	public void removeFromServices(final Product item, final Service value)
	{
		removeFromServices( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrderEntry.totalWeight</code> attribute.
	 * @return the totalWeight - Total Weight of the product
	 */
	public Double getTotalWeight(final SessionContext ctx, final AbstractOrderEntry item)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedAbstractOrderEntry.getTotalWeight requires a session language", 0 );
		}
		return (Double)item.getLocalizedProperty( ctx, TrainingCoreConstants.Attributes.AbstractOrderEntry.TOTALWEIGHT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrderEntry.totalWeight</code> attribute.
	 * @return the totalWeight - Total Weight of the product
	 */
	public Double getTotalWeight(final AbstractOrderEntry item)
	{
		return getTotalWeight( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrderEntry.totalWeight</code> attribute. 
	 * @return the totalWeight - Total Weight of the product
	 */
	public double getTotalWeightAsPrimitive(final SessionContext ctx, final AbstractOrderEntry item)
	{
		Double value = getTotalWeight( ctx,item );
		return value != null ? value.doubleValue() : 0.0d;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrderEntry.totalWeight</code> attribute. 
	 * @return the totalWeight - Total Weight of the product
	 */
	public double getTotalWeightAsPrimitive(final AbstractOrderEntry item)
	{
		return getTotalWeightAsPrimitive( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrderEntry.totalWeight</code> attribute. 
	 * @return the localized totalWeight - Total Weight of the product
	 */
	public Map<Language,Double> getAllTotalWeight(final SessionContext ctx, final AbstractOrderEntry item)
	{
		return (Map<Language,Double>)item.getAllLocalizedProperties(ctx,TrainingCoreConstants.Attributes.AbstractOrderEntry.TOTALWEIGHT,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrderEntry.totalWeight</code> attribute. 
	 * @return the localized totalWeight - Total Weight of the product
	 */
	public Map<Language,Double> getAllTotalWeight(final AbstractOrderEntry item)
	{
		return getAllTotalWeight( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrderEntry.totalWeight</code> attribute. 
	 * @param value the totalWeight - Total Weight of the product
	 */
	public void setTotalWeight(final SessionContext ctx, final AbstractOrderEntry item, final Double value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedAbstractOrderEntry.setTotalWeight requires a session language", 0 );
		}
		item.setLocalizedProperty(ctx, TrainingCoreConstants.Attributes.AbstractOrderEntry.TOTALWEIGHT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrderEntry.totalWeight</code> attribute. 
	 * @param value the totalWeight - Total Weight of the product
	 */
	public void setTotalWeight(final AbstractOrderEntry item, final Double value)
	{
		setTotalWeight( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrderEntry.totalWeight</code> attribute. 
	 * @param value the totalWeight - Total Weight of the product
	 */
	public void setTotalWeight(final SessionContext ctx, final AbstractOrderEntry item, final double value)
	{
		setTotalWeight( ctx, item, Double.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrderEntry.totalWeight</code> attribute. 
	 * @param value the totalWeight - Total Weight of the product
	 */
	public void setTotalWeight(final AbstractOrderEntry item, final double value)
	{
		setTotalWeight( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrderEntry.totalWeight</code> attribute. 
	 * @param value the totalWeight - Total Weight of the product
	 */
	public void setAllTotalWeight(final SessionContext ctx, final AbstractOrderEntry item, final Map<Language,Double> value)
	{
		item.setAllLocalizedProperties(ctx,TrainingCoreConstants.Attributes.AbstractOrderEntry.TOTALWEIGHT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrderEntry.totalWeight</code> attribute. 
	 * @param value the totalWeight - Total Weight of the product
	 */
	public void setAllTotalWeight(final AbstractOrderEntry item, final Map<Language,Double> value)
	{
		setAllTotalWeight( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.weight</code> attribute.
	 * @return the weight - Weight of the product
	 */
	public Double getWeight(final SessionContext ctx, final Product item)
	{
		return (Double)item.getProperty( ctx, TrainingCoreConstants.Attributes.Product.WEIGHT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.weight</code> attribute.
	 * @return the weight - Weight of the product
	 */
	public Double getWeight(final Product item)
	{
		return getWeight( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.weight</code> attribute. 
	 * @return the weight - Weight of the product
	 */
	public double getWeightAsPrimitive(final SessionContext ctx, final Product item)
	{
		Double value = getWeight( ctx,item );
		return value != null ? value.doubleValue() : 0.0d;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.weight</code> attribute. 
	 * @return the weight - Weight of the product
	 */
	public double getWeightAsPrimitive(final Product item)
	{
		return getWeightAsPrimitive( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.weight</code> attribute. 
	 * @param value the weight - Weight of the product
	 */
	public void setWeight(final SessionContext ctx, final Product item, final Double value)
	{
		item.setProperty(ctx, TrainingCoreConstants.Attributes.Product.WEIGHT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.weight</code> attribute. 
	 * @param value the weight - Weight of the product
	 */
	public void setWeight(final Product item, final Double value)
	{
		setWeight( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.weight</code> attribute. 
	 * @param value the weight - Weight of the product
	 */
	public void setWeight(final SessionContext ctx, final Product item, final double value)
	{
		setWeight( ctx, item, Double.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.weight</code> attribute. 
	 * @param value the weight - Weight of the product
	 */
	public void setWeight(final Product item, final double value)
	{
		setWeight( getSession().getSessionContext(), item, value );
	}
	
}
