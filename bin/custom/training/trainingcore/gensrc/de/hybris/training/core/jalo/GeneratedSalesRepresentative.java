/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 13-Jan-2021, 2:23:56 PM                     ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.training.core.jalo;

import de.hybris.platform.constants.CoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.user.Customer;
import de.hybris.platform.util.OneToManyHandler;
import de.hybris.training.core.constants.TrainingCoreConstants;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Generated class for type {@link de.hybris.platform.jalo.GenericItem SalesRepresentative}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSalesRepresentative extends GenericItem
{
	/** Qualifier of the <code>SalesRepresentative.type</code> attribute **/
	public static final String TYPE = "type";
	/** Qualifier of the <code>SalesRepresentative.name</code> attribute **/
	public static final String NAME = "name";
	/** Qualifier of the <code>SalesRepresentative.email</code> attribute **/
	public static final String EMAIL = "email";
	/** Qualifier of the <code>SalesRepresentative.customers</code> attribute **/
	public static final String CUSTOMERS = "customers";
	/**
	* {@link OneToManyHandler} for handling 1:n CUSTOMERS's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<Customer> CUSTOMERSHANDLER = new OneToManyHandler<Customer>(
	CoreConstants.TC.CUSTOMER,
	false,
	"salesRepresentatives",
	null,
	false,
	true,
	CollectionType.SET
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(TYPE, AttributeMode.INITIAL);
		tmp.put(NAME, AttributeMode.INITIAL);
		tmp.put(EMAIL, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesRepresentative.customers</code> attribute.
	 * @return the customers
	 */
	public Set<Customer> getCustomers(final SessionContext ctx)
	{
		return (Set<Customer>)CUSTOMERSHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesRepresentative.customers</code> attribute.
	 * @return the customers
	 */
	public Set<Customer> getCustomers()
	{
		return getCustomers( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesRepresentative.customers</code> attribute. 
	 * @param value the customers
	 */
	public void setCustomers(final SessionContext ctx, final Set<Customer> value)
	{
		CUSTOMERSHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesRepresentative.customers</code> attribute. 
	 * @param value the customers
	 */
	public void setCustomers(final Set<Customer> value)
	{
		setCustomers( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to customers. 
	 * @param value the item to add to customers
	 */
	public void addToCustomers(final SessionContext ctx, final Customer value)
	{
		CUSTOMERSHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to customers. 
	 * @param value the item to add to customers
	 */
	public void addToCustomers(final Customer value)
	{
		addToCustomers( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from customers. 
	 * @param value the item to remove from customers
	 */
	public void removeFromCustomers(final SessionContext ctx, final Customer value)
	{
		CUSTOMERSHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from customers. 
	 * @param value the item to remove from customers
	 */
	public void removeFromCustomers(final Customer value)
	{
		removeFromCustomers( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesRepresentative.email</code> attribute.
	 * @return the email - Email of Sales Representative
	 */
	public String getEmail(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedSalesRepresentative.getEmail requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, EMAIL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesRepresentative.email</code> attribute.
	 * @return the email - Email of Sales Representative
	 */
	public String getEmail()
	{
		return getEmail( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesRepresentative.email</code> attribute. 
	 * @return the localized email - Email of Sales Representative
	 */
	public Map<Language,String> getAllEmail(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,EMAIL,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesRepresentative.email</code> attribute. 
	 * @return the localized email - Email of Sales Representative
	 */
	public Map<Language,String> getAllEmail()
	{
		return getAllEmail( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesRepresentative.email</code> attribute. 
	 * @param value the email - Email of Sales Representative
	 */
	public void setEmail(final SessionContext ctx, final String value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedSalesRepresentative.setEmail requires a session language", 0 );
		}
		setLocalizedProperty(ctx, EMAIL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesRepresentative.email</code> attribute. 
	 * @param value the email - Email of Sales Representative
	 */
	public void setEmail(final String value)
	{
		setEmail( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesRepresentative.email</code> attribute. 
	 * @param value the email - Email of Sales Representative
	 */
	public void setAllEmail(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,EMAIL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesRepresentative.email</code> attribute. 
	 * @param value the email - Email of Sales Representative
	 */
	public void setAllEmail(final Map<Language,String> value)
	{
		setAllEmail( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesRepresentative.name</code> attribute.
	 * @return the name - Name of Sales Representative
	 */
	public String getName(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedSalesRepresentative.getName requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, NAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesRepresentative.name</code> attribute.
	 * @return the name - Name of Sales Representative
	 */
	public String getName()
	{
		return getName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesRepresentative.name</code> attribute. 
	 * @return the localized name - Name of Sales Representative
	 */
	public Map<Language,String> getAllName(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,NAME,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesRepresentative.name</code> attribute. 
	 * @return the localized name - Name of Sales Representative
	 */
	public Map<Language,String> getAllName()
	{
		return getAllName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesRepresentative.name</code> attribute. 
	 * @param value the name - Name of Sales Representative
	 */
	public void setName(final SessionContext ctx, final String value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedSalesRepresentative.setName requires a session language", 0 );
		}
		setLocalizedProperty(ctx, NAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesRepresentative.name</code> attribute. 
	 * @param value the name - Name of Sales Representative
	 */
	public void setName(final String value)
	{
		setName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesRepresentative.name</code> attribute. 
	 * @param value the name - Name of Sales Representative
	 */
	public void setAllName(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,NAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesRepresentative.name</code> attribute. 
	 * @param value the name - Name of Sales Representative
	 */
	public void setAllName(final Map<Language,String> value)
	{
		setAllName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesRepresentative.type</code> attribute.
	 * @return the type - Sales Representative type
	 */
	public EnumerationValue getType(final SessionContext ctx)
	{
		return (EnumerationValue)getProperty( ctx, TYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesRepresentative.type</code> attribute.
	 * @return the type - Sales Representative type
	 */
	public EnumerationValue getType()
	{
		return getType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesRepresentative.type</code> attribute. 
	 * @param value the type - Sales Representative type
	 */
	public void setType(final SessionContext ctx, final EnumerationValue value)
	{
		setProperty(ctx, TYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesRepresentative.type</code> attribute. 
	 * @param value the type - Sales Representative type
	 */
	public void setType(final EnumerationValue value)
	{
		setType( getSession().getSessionContext(), value );
	}
	
}
