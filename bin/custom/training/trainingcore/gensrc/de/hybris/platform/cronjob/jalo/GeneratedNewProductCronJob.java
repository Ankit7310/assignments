/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 13-Jan-2021, 2:23:56 PM                     ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cronjob.jalo;

import de.hybris.platform.cronjob.jalo.CronJob;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.training.core.constants.TrainingCoreConstants;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.cronjob.jalo.NewProductCronJob NewProductCronJob}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedNewProductCronJob extends CronJob
{
	/** Qualifier of the <code>NewProductCronJob.categoryCode</code> attribute **/
	public static final String CATEGORYCODE = "categoryCode";
	/** Qualifier of the <code>NewProductCronJob.thresholdTime</code> attribute **/
	public static final String THRESHOLDTIME = "thresholdTime";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(CronJob.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(CATEGORYCODE, AttributeMode.INITIAL);
		tmp.put(THRESHOLDTIME, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>NewProductCronJob.categoryCode</code> attribute.
	 * @return the categoryCode - Category Code Of Product
	 */
	public Integer getCategoryCode(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, CATEGORYCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>NewProductCronJob.categoryCode</code> attribute.
	 * @return the categoryCode - Category Code Of Product
	 */
	public Integer getCategoryCode()
	{
		return getCategoryCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>NewProductCronJob.categoryCode</code> attribute. 
	 * @return the categoryCode - Category Code Of Product
	 */
	public int getCategoryCodeAsPrimitive(final SessionContext ctx)
	{
		Integer value = getCategoryCode( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>NewProductCronJob.categoryCode</code> attribute. 
	 * @return the categoryCode - Category Code Of Product
	 */
	public int getCategoryCodeAsPrimitive()
	{
		return getCategoryCodeAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>NewProductCronJob.categoryCode</code> attribute. 
	 * @param value the categoryCode - Category Code Of Product
	 */
	public void setCategoryCode(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, CATEGORYCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>NewProductCronJob.categoryCode</code> attribute. 
	 * @param value the categoryCode - Category Code Of Product
	 */
	public void setCategoryCode(final Integer value)
	{
		setCategoryCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>NewProductCronJob.categoryCode</code> attribute. 
	 * @param value the categoryCode - Category Code Of Product
	 */
	public void setCategoryCode(final SessionContext ctx, final int value)
	{
		setCategoryCode( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>NewProductCronJob.categoryCode</code> attribute. 
	 * @param value the categoryCode - Category Code Of Product
	 */
	public void setCategoryCode(final int value)
	{
		setCategoryCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>NewProductCronJob.thresholdTime</code> attribute.
	 * @return the thresholdTime - Threshold Time
	 */
	public String getThresholdTime(final SessionContext ctx)
	{
		return (String)getProperty( ctx, THRESHOLDTIME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>NewProductCronJob.thresholdTime</code> attribute.
	 * @return the thresholdTime - Threshold Time
	 */
	public String getThresholdTime()
	{
		return getThresholdTime( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>NewProductCronJob.thresholdTime</code> attribute. 
	 * @param value the thresholdTime - Threshold Time
	 */
	public void setThresholdTime(final SessionContext ctx, final String value)
	{
		setProperty(ctx, THRESHOLDTIME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>NewProductCronJob.thresholdTime</code> attribute. 
	 * @param value the thresholdTime - Threshold Time
	 */
	public void setThresholdTime(final String value)
	{
		setThresholdTime( getSession().getSessionContext(), value );
	}
	
}
