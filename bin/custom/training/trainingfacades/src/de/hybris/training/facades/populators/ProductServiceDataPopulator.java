package de.hybris.training.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.training.core.model.ServiceModel;
import de.hybris.training.facades.product.data.ServiceData;

public class ProductServiceDataPopulator implements Populator<ServiceModel, ServiceData> {
    @Override
    public void populate(ServiceModel serviceModel, ServiceData serviceData) throws ConversionException {
        serviceData.setServiceType(String.valueOf(serviceModel.getServiceType()));
    }
}