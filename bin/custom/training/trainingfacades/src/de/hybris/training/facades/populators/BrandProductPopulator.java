package de.hybris.training.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductBasicPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang.StringUtils;

public class BrandProductPopulator <SOURCE extends ProductModel, TARGET extends ProductData>
        extends ProductBasicPopulator<SOURCE, TARGET> {
    @Override
    public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException {
        super.populate(productModel, productData);

        if (productModel.getBrand() != null) {

            productData.setBrand(productModel.getBrand());
        }
        else
        {
            productData.setBrand(StringUtils.EMPTY);
        }
        if (productModel.getWeight() != null) {

            productData.setWeight(productModel.getWeight());
        }
        else
        {
            productData.setWeight(0.0);
        }
    }

}
