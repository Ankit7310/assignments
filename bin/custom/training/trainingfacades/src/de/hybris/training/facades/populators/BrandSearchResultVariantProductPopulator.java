package de.hybris.training.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultVariantProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;

public class BrandSearchResultVariantProductPopulator extends SearchResultVariantProductPopulator {
    @Override
    public void populate(SearchResultValueData source, ProductData target) {

        super.populate(source, target);

        // populate brand property values
        target.setBrand(this.<String> getValue(source, "brand"));
        target.setWeight(this.<Double> getValue(source, "weight"));

    }
}