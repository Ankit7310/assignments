package de.hybris.training.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.training.core.model.ServiceModel;
import de.hybris.training.facades.product.data.ServiceData;
import org.springframework.beans.factory.annotation.Required;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class ServiceProductPopulator implements Populator<ProductModel, ProductData> {

    private Converter<ServiceModel, ServiceData> serviceProductConverter;


    @Override
    public void populate(final ProductModel source, final ProductData target) throws ConversionException {
        Set<ServiceData> data = new HashSet<>();
        if (source.getServices() != null) {
            for (ServiceModel serviceModel : source.getServices()) {
                data.add(getServiceProductConverter().convert(serviceModel));
            }
            target.setServiceDataList(data);
        } else {
            fillNullInServiceData(data);
            target.setServiceDataList(data);
        }
    }
    protected void fillNullInServiceData(Set<ServiceData> data){
        ServiceData serviceData = new ServiceData();
        serviceData.setServiceType("None");
        data.add(serviceData);
    }

    public Converter<ServiceModel, ServiceData> getServiceProductConverter() {
        return serviceProductConverter;
    }

    public void setServiceProductConverter(Converter<ServiceModel, ServiceData> serviceProductConverter) {
        this.serviceProductConverter = serviceProductConverter;
    }
    }

